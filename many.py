from main import get_anagrams
from random import choice

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("anagrams", nargs="+")
parser.add_argument("-d", "--database", type=str, default="cache.db")

args = parser.parse_args()
for a in args.anagrams:
    a = a.upper()
    ans = get_anagrams(a, args.database, False)
    if not ans:
        print(a, end=" ")
    else:
        print(choice(ans), end=" ")
print()
