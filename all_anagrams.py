import sqlite3
from argparse import ArgumentParser
from main import get_anagrams
from os.path import exists

parser = ArgumentParser(description="looks for anagrams in the given wordlist")
parser.add_argument("-d", "--database", type=str, default="cache.db")
parser.add_argument("-s", "--search", type=str)

args = parser.parse_args()
if not exists(args.database):
    quit(f"no database found at {args.database}")
con = sqlite3.connect(args.database)
cur = con.cursor()
search_query = args.search or ""
for row in cur.execute("SELECT word FROM words WHERE word LIKE ?", ("%" + search_query + "%",)):
    word = row[0]
    anagrams = get_anagrams(word, args.database, given_word_included=False)
    if anagrams:
        print(f"anagrams to {word}:")
        for a in anagrams:
            print(f"    {a}")

con.close()
