from itertools import permutations
from argparse import ArgumentParser
from os.path import exists

parser = ArgumentParser(description="looks for anagrams in the given wordlist")
parser.add_argument("anagram", type=str)
parser.add_argument("-r", "--wordlist", type=str, default="wordlist.txt")
parser.add_argument("-v", "--verbose", action="store_true")
args = parser.parse_args()
if not exists(args.wordlist):
    quit(f"wordlist {args.wordlist}: no such file")
with open(args.wordlist) as fp:
    print(f"loading wordlist from {fp.name}...", end="")
    wordlist = []
    for w in fp.readlines():
        w = w.replace("\n", "")
        wordlist.append(w.upper())
    print("done")

word = args.anagram
anagrams = []
all_permutations = list(permutations(list(word)))
for permutation in all_permutations:
    anagram = "".join(permutation).upper()
    if anagram in wordlist and anagram not in anagrams:
        anagrams.append(anagram)
        print(anagram)
    elif args.verbose:
        print(anagram, "...nope")
if args.verbose:
    for a in anagrams:
        print(a)
