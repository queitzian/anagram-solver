# Anagram Solver

> [ i ] A python script to find anagrams using sqlite3

## Usage

### `main.py` will find anagrams to a single word

```shell
python3 main.py [--database <path-to-db>] <word>
```

### `cache.py` is used to create the `sqlite3` cache database

```shell
python3 cache.py [-r <path-to-wordlist>] [-o <path-where-to-save-db>] [--force-overwrite] [--verbose]
```

### `all_anagrams.py` will loop across the cache database and look for anagrams using `main.py`

```shell
python3 all_anagrams.py [--database <path-to-db]
```
