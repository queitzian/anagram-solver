import sqlite3
from os.path import exists


def get_anagrams(word: str, db, given_word_included=True) -> list[str]:
    word_sorted = list(word)
    word_sorted.sort()
    word_sorted = ''.join(word_sorted)
    res = []
    if db is not None:
        con = sqlite3.connect(db)
        cur = con.cursor()
        for row in cur.execute(f"SELECT word FROM words WHERE len = ? AND sorted = ?", (len(word), word_sorted)):
            if row[0] == word and not given_word_included:
                continue
            if "__main__" == __name__:
                print(row[0])
            else:
                res.append(row[0])

        con.close()
        return res if not "__main__" == __name__ else None


if "__main__" == __name__:
    from argparse import ArgumentParser

    parser = ArgumentParser(description="looks for anagrams in the given wordlist")
    parser.add_argument("anagram", type=str)
    parser.add_argument("-d", "--database", type=str, default="cache.db")

    args = parser.parse_args()
    inp = args.anagram.upper()
    if not exists(args.database):
        quit(f"no database found at {args.database}")
    get_anagrams(inp, args.database)

