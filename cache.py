from argparse import ArgumentParser
import sqlite3
from os.path import exists
from os import remove

parser = ArgumentParser()
parser.add_argument("-o", "--database", default="cache.db")
parser.add_argument("-r", "--wordlist", default="wordlist.txt")
parser.add_argument("-f", "--force-overwrite", action="store_true")
parser.add_argument("-v", "--verbose", action="store_true")

args = parser.parse_args()
dbpath = args.database
wordlist_path = args.wordlist
if not exists(wordlist_path):
    print(f"no wordlist found at {wordlist_path}.")
    choice = input("Do you want to download a german wordlist to ./wordlist.txt? (y/n)").strip().lower()
    if choice == "y":
        from urllib.request import urlretrieve

        urlretrieve("https://gist.githubusercontent.com/MarvinJWendt/2f4f4154b8ae218600eb091a5706b5f4/raw"
                    "/36b70dd6be330aa61cd4d4cdfda6234dcb0b8784/wordlist-german.txt", "wordlist.txt")
    else:
        quit()
if exists(dbpath):
    if args.force_overwrite:
        remove(dbpath)
    else:
        quit("cachefile already exists")

con = sqlite3.connect(dbpath)
cur = con.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS words(word, len, sorted)")
data: list[tuple] = []

with open(wordlist_path) as fp:
    for word in fp.readlines():
        w = word.replace("\n", "")
        w = w.upper()
        w_sorted = list(w)
        w_sorted.sort()
        row = (
            w,
            len(w),
            ''.join(w_sorted)
        )
        data.append(row)
        if args.verbose:
            print("%s | %d | %s" % row)

cur.executemany("INSERT INTO words VALUES(?,?,?)", data)
con.commit()

con.close()
